package com.pb.creditbhswingclient;

import com.pb.creditbhswingclient.view.main.CreditBHMainFrame;
import javax.swing.JFrame;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {

    public void runApplication() {
        ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
        CreditBHMainFrame calc = new CreditBHMainFrame();
        calc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        calc.setBounds(50, 50, 600, 500);
        calc.setVisible(true);
        calc.pack();
    }

    public static void main(String[] args) {                
        Main main = new Main();
        main.runApplication();
    }

}
