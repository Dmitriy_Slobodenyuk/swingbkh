package com.pb.creditbhswingclient.service;

import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.Credit;
import com.pb.creditbhswingclient.service.interfaces.IClientComponent;
import com.pb.creditbhswingclient.service.interfaces.IServiceModel;
import java.util.List;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;

/**
 * Класс который по заданному критерию получает из БД Информацию по клиентам ,
 * формирует объект и возращает его
 *
 * @author Dmitriy Slobodenyuk
 */
public class ClientJdbcComponent implements IClientComponent {

    private IServiceModel serviceDao;

    public ClientJdbcComponent() {
        serviceDao = new ServiceDao();
    }

    @Override
    public Client getByInn(String inn) {
        Client client = serviceDao.getClientDaoJdbc().getByINN(inn);

        if (client != null) {
            List<Credit> list = serviceDao.getCreditDaoJdbc().getCredits(client.getIdClient());
            client.setListCredits(list);
            return client;
        } else {
            return client;
        }
    }

    @Override
    public Client getByFioAndDateBorn(String firstName, String secondName, String lastName, String dateBorn) {
        Client client = serviceDao.getClientDaoJdbc().getByFIOandDateBorn(firstName, secondName, lastName, dateBorn);
        if (client != null) {
            List<Credit> list = serviceDao.getCreditDaoJdbc().getCredits(client.getIdClient());
            client.setListCredits(list);
            return client;
        } else {
            return client;
        }
    }

    @Override
    public Client getByDataPassport(String sPass, String nPass) {
        Client client = serviceDao.getClientDaoJdbc().getByDataPassport(sPass, nPass);
        if (client != null) {
            List<Credit> list = serviceDao.getCreditDaoJdbc().getCredits(client.getIdClient());
            client.setListCredits(list);
            return client;
        } else {
            return client;
        }
    }

    @Override
    public int addClient(Client client) {
        int result = 0;
        if (serviceDao.getClientDaoJdbc().getByINN(client.getInn()) == null
                && serviceDao.getClientDaoJdbc().getByDataPassport(client.getSerialPassport(),
                        client.getNumberPassport()) == null) {
            result = serviceDao.getClientDaoJdbc().save(client);
        }
        return result;
    }

}
