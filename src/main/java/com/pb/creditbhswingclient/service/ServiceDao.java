package com.pb.creditbhswingclient.service;

import com.pb.creditbhswingclient.model.dao.CatalogDao;
import com.pb.creditbhswingclient.model.dao.ClientDaoJdbc;
import com.pb.creditbhswingclient.model.dao.CreditDaoJdbc;
import com.pb.creditbhswingclient.model.dao.interfaces.IClientDao;
import com.pb.creditbhswingclient.model.dao.interfaces.ICreditDao;
import com.pb.creditbhswingclient.service.interfaces.IServiceModel;


/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ServiceDao implements IServiceModel {

    public ServiceDao() {
    }

    @Override
    public IClientDao getClientDaoJdbc() {
        IClientDao clientDao = new ClientDaoJdbc();
        return clientDao;
    }

    @Override
    public ICreditDao getCreditDaoJdbc() {
        ICreditDao creditDao = new CreditDaoJdbc();
        return creditDao;
    }

    @Override
    public CatalogDao getCatalogDaoJdbc() {
        CatalogDao catalogDao = new CatalogDao();
        return catalogDao;
    }
}
