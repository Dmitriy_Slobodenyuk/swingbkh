package com.pb.creditbhswingclient.service;


import com.pb.creditbhswingclient.model.dao.utils.FormDateFormatter;
import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.ClientWrapper;
import com.pb.creditbhswingclient.service.interfaces.IClientComponent;
import java.util.List;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class ClientRestComponent implements IClientComponent {

    @Override
    public Client getByInn(String inn) {
        Client client = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> response = null;
        try {
            RestTemplate restClient = new RestTemplate();
            clientWrapper = restClient.getForObject("http://localhost:8080/CreditBH/client/client_by_inn/{inn}", ClientWrapper.class, inn);            //            
            response = clientWrapper.getListClients();
            if (response.isEmpty()) {
                client = null;
            } else {
                client = response.get(0);
            }
        } catch (RestClientException ex) {
            System.out.println(ex);
        }
        return client;
    }

    @Override
    public Client getByFioAndDateBorn(String firstName, String secondName, String lastName, String dateBorn) {
        Client client = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> response = null;
        try {
            RestTemplate restClient = new RestTemplate();
            clientWrapper = restClient.getForObject("http://localhost:8080/CreditBH/client/client_by_fio/{fname}/{sname}/{lname}/{bdate}",
                    ClientWrapper.class, firstName, secondName, lastName, FormDateFormatter.parseToLong(dateBorn));            //            
            response = clientWrapper.getListClients();
            if (response.isEmpty()) {
                client = null;
            } else {
                client = response.get(0);
            }
        } catch (RestClientException ex) {
            System.out.println(ex);
        }
        return client;
    }

    @Override
    public Client getByDataPassport(String sPass, String nPass) {
        Client client = null;
        ClientWrapper clientWrapper = new ClientWrapper();
        List<Client> response = null;
        try {
            RestTemplate restClient = new RestTemplate();
            clientWrapper = restClient.getForObject("http://localhost:8080/CreditBH/client/client_by_passport/{ser}/{num}",
                    ClientWrapper.class, sPass, nPass);
            response = clientWrapper.getListClients();//            
            if (response.isEmpty()) {
                client = null;
            } else {
                client = response.get(0);
            }
        } catch (RestClientException ex) {
            System.out.println(ex);
        }
        return client;
    }

    @Override
    public int addClient(Client newClient) {
        int result = 0;

        try {
            RestTemplate rest = new RestTemplate();
            result = rest.postForObject("http://localhost:8080/CreditBH/client/new_client", newClient, Integer.class);
        } catch (RestClientException ex) {
            ex.fillInStackTrace();
        } catch (Exception e) {
            e.fillInStackTrace();
        }

        return result;
    }
}
