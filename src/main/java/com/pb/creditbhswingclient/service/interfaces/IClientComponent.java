package com.pb.creditbhswingclient.service.interfaces;

import com.pb.creditbhswingclient.model.entity.Client;


/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IClientComponent {

    public Client getByInn(String inn);

    public Client getByFioAndDateBorn(String firstName, String secondName,
            String lastName, String dateBorn);

    public Client getByDataPassport(String sPass, String nPass);

    public int addClient(Client client);

}
