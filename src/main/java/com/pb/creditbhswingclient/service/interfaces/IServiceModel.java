package com.pb.creditbhswingclient.service.interfaces;

import com.pb.creditbhswingclient.model.dao.CatalogDao;
import com.pb.creditbhswingclient.model.dao.interfaces.IClientDao;
import com.pb.creditbhswingclient.model.dao.interfaces.ICreditDao;


/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IServiceModel {

    public IClientDao getClientDaoJdbc();

    public ICreditDao getCreditDaoJdbc();

    public CatalogDao getCatalogDaoJdbc();
}
