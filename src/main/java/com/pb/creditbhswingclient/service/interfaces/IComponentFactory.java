package com.pb.creditbhswingclient.service.interfaces;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IComponentFactory {

    public IClientComponent getClientComponent();

    public ICreditComponent getCreditComponent();

    public IClientComponent getClientRestComponent();

    public ICreditComponent getCreditRestComponent();

}
