package com.pb.creditbhswingclient.service.interfaces;

import com.pb.creditbhswingclient.model.entity.Credit;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface ICreditComponent {

    public List<Credit> getById(long id);

    public int updateCreditInfo(long idCredit, String bank, String currency, BigDecimal body,
            Date dateEnd, String delay);

    public int addNewCredit(long idClient, String bankName, BigDecimal startAmount,
            String currencyName, Date dateRegistration,
            Date dateEnd);
}
