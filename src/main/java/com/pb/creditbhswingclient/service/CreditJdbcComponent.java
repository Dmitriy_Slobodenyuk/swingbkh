package com.pb.creditbhswingclient.service;

import com.pb.creditbhswingclient.model.entity.Bank;
import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.Credit;
import com.pb.creditbhswingclient.model.entity.Currency;
import com.pb.creditbhswingclient.service.interfaces.ICreditComponent;
import com.pb.creditbhswingclient.service.interfaces.IServiceModel;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Класс который по заданному критерию получает из БД Информацию по кредитам ,
 * формирует объект и возращает его
 *
 * @author Dimon
 */
public class CreditJdbcComponent implements ICreditComponent {

    private IServiceModel serviceDao;

    public CreditJdbcComponent() {
        serviceDao = new ServiceDao();
    }

    @Override
    public List<Credit> getById(long id) {
        List<Credit> list = serviceDao.getCreditDaoJdbc().getCredits(id);
        return list;
    }

    @Override
    public int updateCreditInfo(long idCredit, String bank, String currency, BigDecimal body, Date dateEnd, String delay) {
        int result = 0;
        Credit credit = new Credit();
        credit.setIdCredit(idCredit);
        Bank newBank = serviceDao.getCatalogDaoJdbc().getBankByName(bank);
        Currency newCurrency = serviceDao.getCatalogDaoJdbc().getCurrencyByName(currency);
        credit.setBank(newBank);
        credit.setCurrency(newCurrency);
        credit.setCreditCurrBody(body);
        credit.setCreditEnd(dateEnd.getTime());
        credit.setOutDelay(delay);
        result = serviceDao.getCreditDaoJdbc().update(credit);
        return result;
    }

    @Override
    public int addNewCredit(long idClient, String bankCode, BigDecimal startAmount,
            String currencyCode, Date dateRegistration,
            Date dateEnd) {

        int result = 0;
        Client client = new Client();
        client.setIdClient(idClient);
        Bank newBank = serviceDao.getCatalogDaoJdbc().getBankByName(bankCode);
        Currency newCurrency = serviceDao.getCatalogDaoJdbc().getCurrencyByName(currencyCode);
        Credit newCredit = new Credit();
        newCredit.setClient(client.getIdClient());
        newCredit.setBank(newBank);
        newCredit.setCurrency(newCurrency);
        newCredit.setBaseAmount(startAmount);
        newCredit.setCreditDateIssue(dateRegistration.getTime());
        //newCredit.setCreditCurrBody(body);
        newCredit.setCreditEnd(dateEnd.getTime());
        result = serviceDao.getCreditDaoJdbc().save(newCredit);
        return result;
    }

}
