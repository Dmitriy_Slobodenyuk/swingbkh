package com.pb.creditbhswingclient.service;

import com.pb.creditbhswingclient.model.entity.Bank;
import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.Credit;
import com.pb.creditbhswingclient.model.entity.CreditWrapper;
import com.pb.creditbhswingclient.model.entity.Currency;
import com.pb.creditbhswingclient.service.interfaces.ICreditComponent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

public class CreditRestComponent implements ICreditComponent {
    @Override
    public List<Credit> getById(long id) {
        List<Credit> credits = null;
        CreditWrapper creditWrapper = null;
        List<Credit> response = null;
        try {
            RestTemplate restClient = new RestTemplate();
            creditWrapper = restClient.getForObject("http://localhost:8080/CreditBH/client/credit_by_id/{id}", CreditWrapper.class, id);            //            
            response = creditWrapper.getList();
            if (response.isEmpty()) {
                credits = null;
            } else {
                credits = response;
            }
        } catch (RestClientException ex) {
            System.out.println(ex);
        }
        return credits;
    }

    @Override
    public int updateCreditInfo(long idCredit, String bankName, String currName, BigDecimal body, Date dateEnd, String delay) {
        int result = 0;

        Credit credit = new Credit();
        credit.setIdCredit(idCredit);
        Bank bank = new Bank();
        bank.setBankName(bankName);
        credit.setBank(bank);
        Currency curr = new Currency();
        curr.setCurrName(currName);
        credit.setCurrency(curr);
        credit.setCreditCurrBody(body);
        credit.setCreditEnd(dateEnd.getTime());
        credit.setOutDelay(delay);

        try {
            RestTemplate rest = new RestTemplate();
            result = rest.postForObject("http://localhost:8080/CreditBH/client/modified_credit", credit, Integer.class);
        } catch (RestClientException ex) {
            ex.fillInStackTrace();
        } catch (Exception e) {
            e.fillInStackTrace();
        }

        return result;
    }

    @Override
    public int addNewCredit(long idClient, String bankName, BigDecimal startAmount, String currencyName, Date dateRegistration, Date dateEnd) {
        int result = 0;
        Client client = new Client();
        client.setIdClient(idClient);
        Bank bank = new Bank();
        bank.setBankName(bankName);
        Currency currency = new Currency();
        currency.setCurrName(currencyName);
        Credit newCredit = new Credit();
        newCredit.setClient(client.getIdClient());
        newCredit.setBank(bank);
        newCredit.setCurrency(currency);
        newCredit.setBaseAmount(startAmount);
        newCredit.setCreditDateIssue(dateRegistration.getTime());        
        newCredit.setCreditEnd(dateEnd.getTime());        

        try {
            RestTemplate rest = new RestTemplate();
            result = rest.postForObject("http://localhost:8080/CreditBH/client/new_credit", newCredit, Integer.class);
        } catch (RestClientException ex) {
            ex.fillInStackTrace();
        } catch (Exception e) {
            e.fillInStackTrace();
        }

        return result;
    }

}
