package com.pb.creditbhswingclient.service;

import com.pb.creditbhswingclient.service.interfaces.IClientComponent;
import com.pb.creditbhswingclient.service.interfaces.IComponentFactory;
import com.pb.creditbhswingclient.service.interfaces.ICreditComponent;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ComponentJdbcFactory implements IComponentFactory {

    @Override
    public IClientComponent getClientComponent() {
        return new ClientJdbcComponent();
    }

    @Override
    public ICreditComponent getCreditComponent() {
        return new CreditJdbcComponent();
    }

    @Override
    public IClientComponent getClientRestComponent() {
        return new ClientRestComponent();
    }

    @Override
    public ICreditComponent getCreditRestComponent() {
        return new CreditRestComponent();
    }

}
