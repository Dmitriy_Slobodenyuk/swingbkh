package com.pb.creditbhswingclient.view.main.panels.form.creditdialog;

import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.Credit;
import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ListCreditPanel extends JPanel {

    private JList list;
    private Client client;
    private DefaultListModel listModel;
    private CreditEditPanel creditEditPanel;

    public ListCreditPanel(final Client client, final CreditEditPanel creditEditPanel) {
        setLayout(new BorderLayout());
        this.creditEditPanel = creditEditPanel;

        this.client = client;
        listModel = new DefaultListModel();
        setListModel();

        list = new JList(listModel);
        list.setSelectionMode(
                ListSelectionModel.SINGLE_SELECTION);
        list.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    // получаем элемент и покажем его
                    int pos = list.locationToIndex(e.getPoint());
                    Credit credit = client.getListCredits().get(pos);
                    creditEditPanel.setCredit(credit);
                    creditEditPanel.getCmbBank().setSelectedItem(credit.getBank().getBankName());
                    creditEditPanel.getCmbCurrency().setSelectedItem(credit.getCurrency().getCurrName());
                    creditEditPanel.getTfCurrentBodyCredit().setText(credit.getCreditCurrBody().toString());
                    creditEditPanel.getDateEndCredit().setValue(credit.getCreditEnd());
                    creditEditPanel.getCmbOutDelay().setSelectedItem(credit.getOutDelay());
                }
            }
        });

        add(new JScrollPane(list), BorderLayout.CENTER);
    }

    private void setListModel() {
        for (Credit credit : client.getListCredits()) {
            listModel.addElement(credit);
        }
    }
}
