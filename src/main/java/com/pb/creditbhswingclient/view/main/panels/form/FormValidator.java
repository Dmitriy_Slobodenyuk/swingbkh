package com.pb.creditbhswingclient.view.main.panels.form;

import com.pb.creditbhswingclient.model.dao.utils.FormDateFormatter;
import java.util.Date;

/**
 * @author Dmitriy Slobodenyuk
 */
public class FormValidator {

    public static boolean isValidateInn(String inn) {
        if (inn.matches("\\d{10}")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidateDataPassport(String serial, String number) {
        if (serial.matches("[А-Я][А-Я]") && number.matches("\\d{6}")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidateDate(String date) {
        if (date.equals("")) {
            return false;
        } else {
            Date dateForm = FormDateFormatter.parse(date);
            Date today = FormDateFormatter.parse(new Date().toString());
            if (dateForm.after(today)) {
                return false;
            } else {
                return true;
            }
        }
    }

    public static boolean isValidateFio(String firstName, String secondName, String lastName) {
        if (lastName.equals("")) {
            if (firstName.matches("[А-ЯA-Z][а-яa-z]{1,25}") && secondName.matches("[А-Я][а-я]{1,25}")) {
                return true;
            } else {
                return false;
            }
        } else {
            if (firstName.matches("[А-Я][а-я]{1,25}") && secondName.matches("[А-Я][а-я]{1,25}") && lastName.matches("[А-Я][а-я]{1,25}")) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static boolean isValidateFormAddClient(String firstName, String secondName, String lastName,
            String dateBorn, String inn, String serialPassport, String numberPassport) {
        if (dateBorn.equals("")) {
            return false;
        } else {

            if (isValidateFio(firstName, secondName, lastName)
                    && isValidateInn(inn) && isValidateDataPassport(serialPassport, numberPassport)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
