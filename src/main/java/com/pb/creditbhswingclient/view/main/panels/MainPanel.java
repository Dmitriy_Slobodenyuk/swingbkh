package com.pb.creditbhswingclient.view.main.panels;

import com.pb.creditbhswingclient.view.main.CreditBHMainFrame;
import com.pb.creditbhswingclient.view.main.panels.form.AddFormClientPanel;
import com.pb.creditbhswingclient.view.main.panels.form.FindFormClientPanel;
import com.pb.creditbhswingclient.view.main.panels.result.ResultOutputPanel;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class MainPanel extends JPanel {

    private CreditBHMainFrame mainFrame;
    private FindFormClientPanel findFormClientPanel;
    private AddFormClientPanel addFormClientPanel;
    private ResultOutputPanel resultOutputPanel;
    
    public MainPanel(CreditBHMainFrame mainFrame) {
        this.mainFrame = mainFrame;
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        init();
    }

    private void init() {
        resultOutputPanel = new ResultOutputPanel();
        addFormClientPanel = new AddFormClientPanel(resultOutputPanel);
        findFormClientPanel = new FindFormClientPanel(mainFrame, resultOutputPanel);
        JTabbedPane formTabs = new JTabbedPane(
                JTabbedPane.TOP);

        JScrollPane scrollPanelFindCliend = new JScrollPane(findFormClientPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        JScrollPane scrollPanelAddClient = new JScrollPane(addFormClientPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        JScrollPane scrollPanelResult = new JScrollPane(resultOutputPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
        // первая панель
        formTabs.addTab("<html><i>Поиск клиента", scrollPanelFindCliend);
        formTabs.addTab("<html><i>Добавить клиента", scrollPanelAddClient);

        // вторая панель
        JTabbedPane outResultTab = new JTabbedPane(JTabbedPane.TOP);
        outResultTab.addTab("<html><i>Результаты поиска клиента", scrollPanelResult);

        // добавляем вкладки в панель содержимого
        setLayout(new GridLayout());
        add(formTabs);
        add(outResultTab);

    }
}
