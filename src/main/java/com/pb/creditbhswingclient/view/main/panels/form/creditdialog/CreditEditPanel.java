package com.pb.creditbhswingclient.view.main.panels.form.creditdialog;

import com.pb.creditbhswingclient.model.dao.utils.FormDateFormatter;
import com.pb.creditbhswingclient.model.entity.Credit;
import com.pb.creditbhswingclient.service.ComponentJdbcFactory;
import com.pb.creditbhswingclient.service.interfaces.IComponentFactory;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CreditEditPanel extends JPanel {

    private final int TEXT_FIELD_SIZE = 35;
    private final int TEXT_FIELD_FONT_SIZE = 20;
    private JComboBox cmbBank;
    private JComboBox cmbCurrency;
    private JComboBox cmbOutDelay;
    private JTextField tfCurrentBodyCredit;
    private JSpinner dateEndCredit;
    private JButton btnSaveEdit;
    private Credit credit;
    private String[] banks = new String[]{"Про Кредит Банк", "Приват Банк", "Райфазен Банк Ав"};
    private String[] currency = new String[]{"Гривна", "Доллар", "Евро"};
    private String[] outDelay = new String[]{"Да", "Нет"};
    private IComponentFactory iComponentFactory;

    public CreditEditPanel() {
        iComponentFactory = new ComponentJdbcFactory();
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new BorderLayout());
        init();
    }

    private void init() {
        JPanel pEditForm = new JPanel(new BorderLayout());
        pEditForm.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Кредит :"), new EmptyBorder(5, 5, 5, 5)));
        cmbOutDelay = new JComboBox(outDelay);
        cmbBank = new JComboBox(banks);
        cmbBank.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        cmbCurrency = new JComboBox(currency);
        cmbCurrency.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfCurrentBodyCredit = new JTextField();
        tfCurrentBodyCredit.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        // модель для выбора даты
        SpinnerModel month = new SpinnerDateModel(
                new Date(), null, null, Calendar.MONTH);
        dateEndCredit = new JSpinner(month);
        dateEndCredit.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        JPanel panel = new JPanel(new GridLayout(6, 2, 2, 2));
        panel.add(new JLabel("Банк:"));
        panel.add(cmbBank);
        panel.add(new JLabel("Валюта:"));
        panel.add(cmbCurrency);
        panel.add(new JLabel("Тело кредита:"));
        panel.add(tfCurrentBodyCredit);
        panel.add(new JLabel("Дата погашения:"));
        panel.add(dateEndCredit);
        panel.add(new JLabel("Просрочки:"));
        panel.add(cmbOutDelay);
        btnSaveEdit = new JButton("Сохранить");
        btnSaveEdit.addActionListener(new ButtonSaveAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnSaveEdit);

        pEditForm.add(panel, BorderLayout.NORTH);
        pEditForm.add(flow, BorderLayout.SOUTH);
        add(pEditForm, BorderLayout.NORTH);
    }

    public JComboBox getCmbBank() {
        return cmbBank;
    }

    public void setCmbBank(JComboBox cmbBank) {
        this.cmbBank = cmbBank;
    }

    public JComboBox getCmbCurrency() {
        return cmbCurrency;
    }

    public void setCmbCurrency(JComboBox cmbCurrency) {
        this.cmbCurrency = cmbCurrency;
    }

    public JTextField getTfCurrentBodyCredit() {
        return tfCurrentBodyCredit;
    }

    public void setTfCurrentBodyCredit(JTextField tfCurrentBodyCredit) {
        this.tfCurrentBodyCredit = tfCurrentBodyCredit;
    }

    public JSpinner getDateEndCredit() {
        return dateEndCredit;
    }

    public void setDateEndCredit(JSpinner dateEndCredit) {
        this.dateEndCredit = dateEndCredit;
    }

    public JComboBox getCmbOutDelay() {
        return cmbOutDelay;
    }

    public void setCmbOutDelay(JComboBox cmbOutDelay) {
        this.cmbOutDelay = cmbOutDelay;
    }

    public JButton getBtnSaveEdit() {
        return btnSaveEdit;
    }

    public void setBtnSaveEdit(JButton btnSaveEdit) {
        this.btnSaveEdit = btnSaveEdit;
    }

    public Credit getCredit() {
        return credit;
    }

    public void setCredit(Credit credit) {
        this.credit = credit;
    }

    private class ButtonSaveAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();
            if (credit != null) {
                if (!tfCurrentBodyCredit.getText().equals("")) {
                    //get text with form and set credit object and save
                    int res = iComponentFactory.getCreditRestComponent().updateCreditInfo(credit.getIdCredit(),
                            cmbBank.getSelectedItem().toString(),
                            cmbCurrency.getSelectedItem().toString(),
                            BigDecimal.valueOf(Double.valueOf(tfCurrentBodyCredit.getText())),
                            FormDateFormatter.format(dateEndCredit.getValue()),
                            cmbOutDelay.getSelectedItem().toString());
                    if (res == 1) {
                        JOptionPane.showMessageDialog(btnSaveEdit, "Кредит успешно обновлен !");
                    } else {
                        JOptionPane.showMessageDialog(btnSaveEdit, "Обновление не прошло !");
                    }
                } else {
                    JOptionPane.showMessageDialog(btnSaveEdit, "Поле с телом кредита не должно быть пустым !");
                }
            }
        }
    }
}
