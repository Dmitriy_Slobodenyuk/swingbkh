package com.pb.creditbhswingclient.view.main.panels.result;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author Slobodenyuk Dmitriy
 */
public class MessageResultPanel extends JPanel {

    private String message;
    private boolean successOperation;

    public MessageResultPanel(String msg, boolean successOperation) {
        this.message = msg;
        this.successOperation = successOperation;
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setBackground(Color.BLACK);
        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(0, 100));
        init();
    }

    private void init() {
        Icon icon = null;
        if (successOperation) {
            icon = new ImageIcon("src/main/resources/success_icon.png");
        } else {
            icon = new ImageIcon("src/main/resources/error_icon.png");
        }
        JLabel label = new JLabel(icon);
        JPanel pIcon = new JPanel(new FlowLayout(FlowLayout.LEFT));
        pIcon.setBackground(Color.BLACK);
        pIcon.add(label);
        add(pIcon, BorderLayout.WEST);

        JTextArea note = new JTextArea(message);
        note.setBackground(Color.BLACK);
        note.setForeground(Color.white);
        note.setFont(new Font("Times New Roman", 0, 16));
        Insets margin = note.getMargin();
        margin.left = 35;
        note.setMargin(margin);
        note.setEditable(false);

        JPanel pClientInfo = new JPanel(new BorderLayout());
        pClientInfo.setBackground(Color.BLACK);
        pClientInfo.add(note, BorderLayout.WEST);

        add(pClientInfo, BorderLayout.CENTER);
    }
}
