package com.pb.creditbhswingclient.view.main.panels.result;

import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.view.main.panels.form.creditdialog.CreditDialog;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 * Панель для вывода информации по найденному клиенту
 * @author Slobodenyuk Dmitriy
 */
public class ListItemResultPanel extends JPanel {
    
    private Client client;
    private JFrame parrent;
    
    public ListItemResultPanel(Client client, JFrame parent) {
        this.client = client;
        this.parrent = parent;
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new GridLayout(1, 2, 15, 20));
        setPreferredSize(new Dimension(0, 250));
        init();
    }
    
    private void init() {
        Icon icon = new ImageIcon("src/main/resources/ava_def.png");
        JLabel label = new JLabel(icon);
        
        JPanel pPhoto = new JPanel();
        pPhoto.add(label);
        add(pPhoto);
        
        JPanel pClientInfo = new JPanel(new BorderLayout());
        JPanel p = new JPanel(new GridLayout(5, 1, 10, 10));
        
        p.add(new JLabel("ФИО: " + client.getFirstName() + " " + client.getSecondName() + " " + client.getLastName()));
        p.add(new JLabel("ИНН: " + client.getInn()));
        p.add(new JLabel("Дата Рождения: " + client.getDateBornToString()));
        p.add(new JLabel("Паспорт: " + client.getSerialPassport() + " " + client.getNumberPassport()));
        
        // кнопка просмотра кредитов клиента
        JButton btnCredit = new JButton("Посмотреть кредиты");
        btnCredit.addActionListener(new CreditShowAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.LEFT));
        flow.add(btnCredit);
        p.add(flow);
        pClientInfo.add(p, BorderLayout.NORTH);
        add(pClientInfo);
    }
    
    private class CreditShowAction implements ActionListener {        
        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();
            
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    CreditDialog credit = new CreditDialog(parrent, client);
                    credit.setVisible(true);
                }
            });
        }
    }
}
