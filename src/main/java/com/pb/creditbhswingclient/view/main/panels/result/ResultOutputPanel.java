package com.pb.creditbhswingclient.view.main.panels.result;

import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ResultOutputPanel extends JPanel {

    private ListItemResultPanel listItemResultPanel;

    public ResultOutputPanel() {
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new BorderLayout());
        setBackground(Color.white);
        init();
    }

    private void init() {

    }
}
