package com.pb.creditbhswingclient.view.main.panels.form.creditdialog;

import com.pb.creditbhswingclient.model.entity.Client;
import java.awt.Dimension;
import javax.swing.BorderFactory;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CreditDialog extends JDialog {

    private CreditMainPanel creditMainPanel;
    private Client client;

    public CreditDialog(JFrame parrent, Client client) {
        super(parrent, "Кредиты клиента", true);
        this.client = client;
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocation(300, 100);
        setMinimumSize(new Dimension(700, 400));
        setResizable(false);
        init();
        pack();
    }

    private void init() {
        creditMainPanel = new CreditMainPanel(client);
        add(creditMainPanel);
    }

    private class ListCredits extends JPanel {

        public ListCredits() {
            setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        }
    }
}
