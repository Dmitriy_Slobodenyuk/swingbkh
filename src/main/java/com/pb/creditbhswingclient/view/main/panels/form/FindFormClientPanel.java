package com.pb.creditbhswingclient.view.main.panels.form;

import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.service.ComponentJdbcFactory;
import com.pb.creditbhswingclient.service.interfaces.IComponentFactory;
import com.pb.creditbhswingclient.view.main.CreditBHMainFrame;
import com.pb.creditbhswingclient.view.main.panels.result.ListItemResultPanel;
import com.pb.creditbhswingclient.view.main.panels.result.MessageResultPanel;
import com.pb.creditbhswingclient.view.main.panels.result.ResultOutputPanel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 * Формы для поиска клиентов 
 * @author Dmitriy Slobodenyuk
 */
public class FindFormClientPanel extends JPanel {

    private final int TEXT_FIELD_SIZE = 35;
    private final int TEXT_FIELD_FONT_SIZE = 20;
    private CreditBHMainFrame mainFrame;

    private ResultOutputPanel resultOutputPanel;
    private JTextField tfFirstName;
    private JTextField tfSecondName;
    private JTextField tfLastName;
    private JSpinner tfDateBorn;
    private JButton btnFindFormFio;

    private JTextField tfInn;
    private JButton btnFindFormInn;

    private JTextField tfSerialPassport;
    private JTextField tfNumberPassport;
    private JButton btnFindFormPassport;
    private IComponentFactory iComponentFactory;

    public FindFormClientPanel(CreditBHMainFrame mainFrame, ResultOutputPanel resultOutputPanel) {
        this.mainFrame = mainFrame;
        this.resultOutputPanel = resultOutputPanel;
        iComponentFactory = new ComponentJdbcFactory();
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        initFindByFormFio();
        initFindFormByInn();
        initFindFormByDataPassport();
    }

    /**
     * Иннициализация компонентов формы поиска клиента по ИНН
     */
    private void initFindFormByInn() {
        JPanel pFindByInn = new JPanel(new BorderLayout());
        pFindByInn.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Поиск по ИНН"), new EmptyBorder(5, 5, 5, 5)));

        tfInn = new JTextField();
        //tfInn.setPreferredSize(new Dimension(50, TEXT_FIELD_SIZE));
        tfInn.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        JPanel panel = new JPanel(new GridLayout(1, 2, 2, 2));
        panel.add(new JLabel("Инн:"));
        panel.add(tfInn);

        btnFindFormInn = new JButton("Найти");
        btnFindFormInn.setName("innbtn");
        btnFindFormInn.addActionListener(new InnFormAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnFindFormInn);

        pFindByInn.add(panel, BorderLayout.NORTH);
        pFindByInn.add(flow, BorderLayout.CENTER);

        //add(pFindByInn, BorderLayout.CENTER);
        setAlignmentX(CENTER_ALIGNMENT);
        add(pFindByInn);

    }

    /**
     * Иннициализация компонентов формы поиска клиента по ФИО и Дате рождения
     */
    private void initFindByFormFio() {
        JPanel pFindByFio = new JPanel(new BorderLayout());
        pFindByFio.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Поиск по ФИО"), new EmptyBorder(5, 5, 5, 5)));

        tfFirstName = new JTextField();
        tfFirstName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        tfSecondName = new JTextField();
        tfSecondName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfLastName = new JTextField();
        tfLastName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        // настраиваем поле выбора даты
        Calendar cal = Calendar.getInstance();
        Date date = cal.getTime();
        SpinnerDateModel spinner = new SpinnerDateModel();
        spinner.setValue(date);
        tfDateBorn = new JSpinner(spinner);
        tfDateBorn.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        JPanel panel = new JPanel(new GridLayout(4, 2, 2, 2));
        panel.add(new JLabel("Имя:"));
        panel.add(tfFirstName);
        panel.add(new JLabel("Фамилия:"));
        panel.add(tfSecondName);
        panel.add(new JLabel("Отчество:"));
        panel.add(tfLastName);
        panel.add(new JLabel("Дата рождения:"));
        panel.add(tfDateBorn);
        btnFindFormFio = new JButton("Найти");
        btnFindFormFio.addActionListener(new FioAndDateBornFormAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnFindFormFio);
        pFindByFio.add(panel, BorderLayout.NORTH);
        pFindByFio.add(flow, BorderLayout.SOUTH);
//        add(pFindByFio, BorderLayout.NORTH);
        setAlignmentX(CENTER_ALIGNMENT);
        add(pFindByFio);

    }

    /**
     * Иннициализация компонентов формы поиска клиента по серии и номеру
     * паспорта
     */
    private void initFindFormByDataPassport() {
        JPanel pFindByPassport = new JPanel(new BorderLayout());
        pFindByPassport.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Поиск по паспорту"), new EmptyBorder(5, 5, 5, 5)));

        tfSerialPassport = new JTextField();
        tfSerialPassport.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfNumberPassport = new JTextField();
        tfNumberPassport.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        JPanel panel = new JPanel(new GridLayout(2, 2, 2, 2));
        panel.add(new JLabel("Серия:"));
        panel.add(tfSerialPassport);
        panel.add(new JLabel("Номер:"));
        panel.add(tfNumberPassport);

        btnFindFormPassport = new JButton("Найти");
        btnFindFormPassport.addActionListener(new PassportDataFormAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnFindFormPassport);
        pFindByPassport.add(panel, BorderLayout.NORTH);
        pFindByPassport.add(flow, BorderLayout.SOUTH);
        setAlignmentX(CENTER_ALIGNMENT);
        add(pFindByPassport);

    }

    private class InnFormAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (FormValidator.isValidateInn(tfInn.getText())) {
                            Client client = null;
                            List<Client> listClients = null;
                            client = iComponentFactory.getClientRestComponent().getByInn(tfInn.getText());
                            if (client != null) {
                                resultOutputPanel.removeAll();
                                resultOutputPanel.updateUI();
                                ListItemResultPanel pResult = new ListItemResultPanel(client, mainFrame);
                                resultOutputPanel.add(pResult, BorderLayout.NORTH);
                                resultOutputPanel.updateUI();
                            } else {
                                showMessage("По вашему запросу ничего не найдено !");
                            }
                        } else {
                            showMessage("Ошибка ввода ! Инн - 10 цифр");
                        }
                    } catch (Exception e) {
                        Thread t = Thread.currentThread();
                        t.getUncaughtExceptionHandler().uncaughtException(t, e);
                        showMessage("Ошибка подключения !");
                        System.out.println(e);
                    }

                }
            }
            );
        }
    }

    private class PassportDataFormAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (FormValidator.isValidateDataPassport(tfSerialPassport.getText(), tfNumberPassport.getText())) {
                            Client client = null;
                            client = iComponentFactory.getClientRestComponent().getByDataPassport(tfSerialPassport.getText(), tfNumberPassport.getText());
                            if (client != null) {
                                resultOutputPanel.removeAll();
                                resultOutputPanel.updateUI();
                                ListItemResultPanel pResult = new ListItemResultPanel(client, mainFrame);
                                resultOutputPanel.add(pResult, BorderLayout.NORTH);
                                resultOutputPanel.updateUI();
                            } else {
                                showMessage("По вашему запросу ничего не найдено !");
                            }
                        } else {
                            showMessage("Ошибка ввода ! Серия - 2 цифры, номер - 6 цифр");
                        }
                    } catch (Exception e) {
                        Thread t = Thread.currentThread();
                        t.getUncaughtExceptionHandler().uncaughtException(t, e);
                        showMessage("Ошибка подключения !");
                        System.out.println(e);
                    }

                }
            }
            );
        }
    }

    private class FioAndDateBornFormAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();

            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (FormValidator.isValidateFio(tfFirstName.getText(), tfSecondName.getText(), tfLastName.getText())) {
                            Client client = null;
                            client = iComponentFactory.getClientRestComponent().getByFioAndDateBorn(tfFirstName.getText(),
                                    tfSecondName.getText(), tfLastName.getText(),
                                    new SimpleDateFormat("yyyy-dd-MM").format(tfDateBorn.getValue()));
                            if (client != null) {
                                resultOutputPanel.removeAll();
                                resultOutputPanel.updateUI();
                                ListItemResultPanel pResult = new ListItemResultPanel(client, mainFrame);
                                resultOutputPanel.add(pResult, BorderLayout.NORTH);
                                resultOutputPanel.updateUI();
                            } else {
                                showMessage("По вашему запросу ничего не найдено !");
                            }

                        } else {
                            showMessage("Ошибка ввода ! ФИО должны начинаться с заглавной буквы !");
                        }
                    } catch (Exception e) {
                        Thread t = Thread.currentThread();
                        t.getUncaughtExceptionHandler().uncaughtException(t, e);
                        showMessage("Ошибка подключения !");
                        System.out.println(e);
                    }
                }
            }
            );
        }
    }

    private void showMessage(String message) {
        resultOutputPanel.removeAll();
        resultOutputPanel.updateUI();
        MessageResultPanel msg = new MessageResultPanel(message, false);
        resultOutputPanel.add(msg, BorderLayout.NORTH);
        resultOutputPanel.updateUI();
    }
}
