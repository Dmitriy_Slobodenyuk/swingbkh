package com.pb.creditbhswingclient.view.main.panels.form;

import com.pb.creditbhswingclient.model.dao.utils.FormDateFormatter;
import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.service.ComponentJdbcFactory;
import com.pb.creditbhswingclient.service.interfaces.IComponentFactory;
import com.pb.creditbhswingclient.view.main.panels.result.MessageResultPanel;
import com.pb.creditbhswingclient.view.main.panels.result.ResultOutputPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;


/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class AddFormClientPanel extends JPanel {

    private final int TEXT_FIELD_SIZE = 35;
    private final int TEXT_FIELD_FONT_SIZE = 20;
    private final String NOTE = "Примечание:\n- Поля не должны быть пустыми\n"
            + "- ФИО должны начинаться с заглавной буквы\n- Инн это 10 цыфр\n"
            + "- Серия паспорта это две заглавные буквы\n- Номер паспорта это 6 цыфр";

    private JTextField tfFirstName;
    private JTextField tfSecondName;
    private JTextField tfLastName;
    private JTextField tfInn;
    private JSpinner tfDateBorn;
    private JTextField tfSerialPassport;
    private JTextField tfNumberPassport;
    private JButton btnAddFormClient;
    
    private ResultOutputPanel resultOutputPanel;
    private IComponentFactory iComponentFactory;

    public AddFormClientPanel(ResultOutputPanel resultOutputPanel) {
        this.resultOutputPanel = resultOutputPanel;
        iComponentFactory = new ComponentJdbcFactory();
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new BorderLayout());
        init();
    }

    private void init() {
        JPanel pNote = new JPanel(new BorderLayout());
        JPanel pAddClient = new JPanel(new BorderLayout());
        pAddClient.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Добавить клиента"), new EmptyBorder(5, 5, 5, 5)));

        tfFirstName = new JTextField();
        //tfFirstName.setPreferredSize(new Dimension(50, TEXT_FIELD_SIZE));
        tfFirstName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfSecondName = new JTextField();
        tfSecondName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfLastName = new JTextField();
        tfLastName.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        // модель для выбора даты
        SpinnerModel month = new SpinnerDateModel(
                new Date(), null, null, Calendar.MONTH);
        tfDateBorn = new JSpinner(month);
        tfDateBorn.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        tfInn = new JTextField();
        tfInn.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        tfSerialPassport = new JTextField();
        tfSerialPassport.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfNumberPassport = new JTextField();
        tfNumberPassport.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        JPanel panel = new JPanel(new GridLayout(7, 2, 2, 2));
        panel.add(new JLabel("Имя:"));
        panel.add(tfFirstName);
        panel.add(new JLabel("Фамилия:"));
        panel.add(tfSecondName);
        panel.add(new JLabel("Отчество:"));
        panel.add(tfLastName);
        panel.add(new JLabel("Дата рождения:"));
        panel.add(tfDateBorn);
        panel.add(new JLabel("Инн:"));
        panel.add(tfInn);
        panel.add(new JLabel("Серия паспорта:"));
        panel.add(tfSerialPassport);
        panel.add(new JLabel("Номер паспорта:"));
        panel.add(tfNumberPassport);
        btnAddFormClient = new JButton("Добавить");
        btnAddFormClient.addActionListener(new AddClientFormAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnAddFormClient);
        pAddClient.add(panel, BorderLayout.NORTH);
        pAddClient.add(flow, BorderLayout.SOUTH);

        JPanel flowNote = new JPanel(new BorderLayout());
        JTextArea note = new JTextArea(NOTE);
        note.setBackground(new Color(238, 238, 238));
        note.setFont(new Font("Times New Roman", 0, 16));
        Insets margin = note.getMargin();
        margin.top = 15;
        note.setMargin(margin);
        note.setEditable(false);
        flowNote.add(note, BorderLayout.NORTH);
        pNote.add(flowNote, BorderLayout.CENTER);
        add(pAddClient, BorderLayout.NORTH);
        add(pNote, BorderLayout.CENTER);
    }

    private class AddClientFormAction implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    int resultSave = 0;
                    if (FormValidator.isValidateFormAddClient(tfFirstName.getText(), tfSecondName.getText(),
                            tfLastName.getText(), new SimpleDateFormat("yyyy-dd-MM").format(tfDateBorn.getValue()),
                            tfInn.getText(), tfSerialPassport.getText(), tfNumberPassport.getText())) {

                        Client client = new Client();
                        client.setFirstName(tfFirstName.getText());
                        client.setSecondName(tfSecondName.getText());
                        client.setLastName(tfLastName.getText());
                        client.setDateBorn(FormDateFormatter.format(tfDateBorn.getValue()).getTime());
                        client.setInn(tfInn.getText());
                        client.setSerialPassport(tfSerialPassport.getText());
                        client.setNumberPassport(tfNumberPassport.getText());

                        resultSave = iComponentFactory.getClientRestComponent().addClient(client);
                        if (resultSave == 1) {
                            showMessage("Клиент " + client.getSecondName() + " " + client.getFirstName() + " добавлен !", true);
                        } else {
                            showMessage("Ошибка возможно уже существует клиент с такими данными !", false);
                        }
                    } else {
                        showMessage("Неправильный ввод !", false);
                    }

                }
            }
            );
        }
    }

    private void showMessage(String message, boolean successOperation) {
        resultOutputPanel.removeAll();
        resultOutputPanel.updateUI();
        MessageResultPanel msg = new MessageResultPanel(message, successOperation);
        resultOutputPanel.add(msg, BorderLayout.NORTH);
        resultOutputPanel.updateUI();
    }
}
