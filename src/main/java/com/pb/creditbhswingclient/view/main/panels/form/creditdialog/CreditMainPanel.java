package com.pb.creditbhswingclient.view.main.panels.form.creditdialog;

import com.pb.creditbhswingclient.model.entity.Client;
import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CreditMainPanel extends JPanel {

    private Client client;
    private ListCreditPanel listCreditPanel;
    private CreditEditPanel creditEditPanel;
    private AddCreditPanel addCreditPanel;

    public CreditMainPanel(Client client) {
        this.client = client;
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        init();
    }

    private void init() {
        addCreditPanel = new AddCreditPanel(client.getIdClient());
        creditEditPanel = new CreditEditPanel();
        listCreditPanel = new ListCreditPanel(client, creditEditPanel);

        JTabbedPane formTabs = new JTabbedPane(
                JTabbedPane.TOP);
        formTabs.addTab("<html><i>Кредиты клиента", listCreditPanel);

        JTabbedPane outResultTab = new JTabbedPane(JTabbedPane.TOP);
        outResultTab.addTab("<html><i>Редактирование", creditEditPanel);
        outResultTab.addTab("<html><i>Добавление", addCreditPanel);
        setLayout(new GridLayout());
        add(formTabs);
        add(outResultTab);
    }
}
