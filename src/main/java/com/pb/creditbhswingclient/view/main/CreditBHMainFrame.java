package com.pb.creditbhswingclient.view.main;

import com.pb.creditbhswingclient.view.main.panels.MainPanel;
import java.awt.Dimension;
import javax.swing.JFrame;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CreditBHMainFrame extends JFrame {

    public CreditBHMainFrame() {
        setTitle("Бюро Кредитных Историй");
        MainPanel panel = new MainPanel(this);
        setMinimumSize(new Dimension(1000, 600));
        add(panel);
        pack();
    }
}
