package com.pb.creditbhswingclient.view.main.panels.form.creditdialog;

import com.pb.creditbhswingclient.model.dao.utils.FormDateFormatter;
import com.pb.creditbhswingclient.service.ComponentJdbcFactory;
import com.pb.creditbhswingclient.service.interfaces.IComponentFactory;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerModel;
import javax.swing.SwingUtilities;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class AddCreditPanel extends JPanel {

    private final int TEXT_FIELD_SIZE = 35;
    private final int TEXT_FIELD_FONT_SIZE = 20;

    private JComboBox cmbBank;
    private JTextField tfStartAmount;
    private JComboBox cmbCurrency;
    private JSpinner dateStartCredit;
    //private JTextField tfCurrentBody;
    private JSpinner dateEndCredit;
    //private JComboBox cmbOutDelay;
    private JButton btnSaveCredit;

    private IComponentFactory iComponentFactory;
    private String[] banks = new String[]{"Про Кредит Банк", "Приват Банк", "Райфазен Банк Ав"};
    private String[] currency = new String[]{"Гривна", "Доллар", "Евро"};
    private String[] outDelay = new String[]{"Да", "Нет"};
    long idClient;

    public AddCreditPanel(long idClient) {
        this.idClient = idClient;
        iComponentFactory = new ComponentJdbcFactory();
        setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 12));
        setLayout(new BorderLayout());
        init();
    }

    private void init() {
        JPanel pAddCredit = new JPanel(new BorderLayout());
        pAddCredit.setBorder(new CompoundBorder(new TitledBorder("<html><i style=\"font-size:14px\">Добавить кредит"), new EmptyBorder(5, 5, 5, 5)));

        cmbBank = new JComboBox(banks);
        cmbBank.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        tfStartAmount = new JTextField();
        tfStartAmount.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        cmbCurrency = new JComboBox(currency);
        cmbCurrency.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        // модель для выбора даты
        SpinnerModel monthStart = new SpinnerDateModel(
                new Date(), null, null, Calendar.MONTH);
        dateStartCredit = new JSpinner(monthStart);
        dateStartCredit.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
//        tfCurrentBody = new JTextField();
//        tfCurrentBody.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
        SpinnerModel monthEnd = new SpinnerDateModel(
                new Date(), null, null, Calendar.MONTH);
        dateEndCredit = new JSpinner(monthEnd);
        dateEndCredit.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));
//        cmbOutDelay = new JComboBox(outDelay);
//        cmbOutDelay.setFont(new Font("Times New Roman", 0, TEXT_FIELD_FONT_SIZE));

        JPanel panel = new JPanel(new GridLayout(7, 2, 2, 2));
        panel.add(new JLabel("Банк:"));
        panel.add(cmbBank);
        panel.add(new JLabel("Валюта:"));
        panel.add(cmbCurrency);
        panel.add(new JLabel("Взнос:"));
        panel.add(tfStartAmount);
        panel.add(new JLabel("Дата оформления:"));
        panel.add(dateStartCredit);
//        panel.add(new JLabel("Текущая сумма:"));
//        panel.add(tfCurrentBody);
        panel.add(new JLabel("Дата закрытия:"));
        panel.add(dateEndCredit);
//        panel.add(new JLabel("Просрочки:"));
//        panel.add(cmbOutDelay);
        btnSaveCredit = new JButton("Добавить");
        btnSaveCredit.addActionListener(new CreditSaveFormAction());
        JPanel flow = new JPanel(
                new FlowLayout(FlowLayout.RIGHT));
        flow.add(btnSaveCredit);
        pAddCredit.add(panel, BorderLayout.NORTH);
        pAddCredit.add(flow, BorderLayout.SOUTH);

        add(pAddCredit, BorderLayout.NORTH);
    }

    private class CreditSaveFormAction implements ActionListener {

        public void actionPerformed(ActionEvent event) {
            String command = event.getActionCommand();
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (tfStartAmount.getText().equals("")) {
                        JOptionPane.showMessageDialog(btnSaveCredit, "Поля взнос и текущая сумма не должны быть пустыми !");
                    } else {
                        int res = iComponentFactory.getCreditRestComponent().addNewCredit(
                                idClient, cmbBank.getSelectedItem().toString(),
                                BigDecimal.valueOf(Double.valueOf(tfStartAmount.getText())),
                                cmbCurrency.getSelectedItem().toString(),
                                FormDateFormatter.format(dateStartCredit.getValue()),
                                FormDateFormatter.format(dateEndCredit.getValue())
                        );
                        if (res == 1) {
                            JOptionPane.showMessageDialog(btnSaveCredit, "Кредит успешно добавлен !");
                        } else {
                            JOptionPane.showMessageDialog(btnSaveCredit, "Добавление не прошло !");
                        }
                    }
                }
            }
            );
        }
    }
}
