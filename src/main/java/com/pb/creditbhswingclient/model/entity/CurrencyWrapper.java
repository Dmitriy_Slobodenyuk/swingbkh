package com.pb.creditbhswingclient.model.entity;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "currencies")
public class CurrencyWrapper {

    List<Currency> list = new ArrayList<Currency>();

    public CurrencyWrapper() {
    }

    public CurrencyWrapper(List<Currency> list) {
        this.list = list;
    }

    @XmlElement(name = "currency")
    public List<Currency> getList() {
        return list;
    }

    public void setList(List<Currency> list) {
        this.list = list;
    }

}
