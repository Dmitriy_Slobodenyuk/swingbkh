package com.pb.creditbhswingclient.model.entity;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
@XmlRootElement()
public class Currency implements Serializable {

    private String currCode;
    private String currName;

    public Currency() {

    }

    public Currency(String currCode, String currName) {
        this.currCode = currCode;
        this.currName = currName;
    }

    public String getCurrCode() {
        return currCode;
    }

    public void setCurrCode(String currCode) {
        this.currCode = currCode;
    }

    public String getCurrName() {
        return currName;
    }

    public void setCurrName(String currName) {
        this.currName = currName;
    }
}
