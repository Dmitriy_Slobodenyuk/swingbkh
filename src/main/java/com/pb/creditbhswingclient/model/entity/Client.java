package com.pb.creditbhswingclient.model.entity;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Entity Client
 *
 * @author Dmitriy Slobodenyuk
 */
@XmlRootElement()
public class Client implements Serializable {

    private long idClient;
    private String inn;
    private String firstName;
    private String secondName;
    private String lastName;
    private long dateBorn;
    private String serialPassport;
    private String numberPassport;
    private String image = "/resources/image/ava_def.png";
    private List<Credit> listCredits;
    private String dateFormat = "yyyy-dd-MM";

    public Client() {
    }

    public Client(int idClient, String inn, String firstName, String secondName,
            String lastName, long dateBorn, String serialPassport, String numberPassport) {
        this.idClient = idClient;
        this.inn = inn;
        this.firstName = firstName;
        this.secondName = secondName;
        this.lastName = lastName;
        this.dateBorn = dateBorn;
        this.serialPassport = serialPassport;
        this.numberPassport = numberPassport;
    }

    public long getIdClient() {
        return idClient;
    }

    public void setIdClient(long idClient) {
        this.idClient = idClient;
    }

    public String getInn() {
        return inn;
    }

    public void setInn(String inn) {
        this.inn = inn;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {

        this.lastName = lastName;
    }
    public String getDateBornToString() {
        String date = "";
        Date d = new Date(dateBorn);
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        date = formatter.format(d);
        return date;
    }

    public long getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(long dateBorn) {
        this.dateBorn = dateBorn;
    }

    public String getSerialPassport() {
        return serialPassport;
    }

    public void setSerialPassport(String serialPassport) {
        this.serialPassport = serialPassport;
    }

    public String getNumberPassport() {
        return numberPassport;
    }

    public void setNumberPassport(String numberPassport) {
        this.numberPassport = numberPassport;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        if (image != null) {
            this.image = image;
        }
    }

    @XmlElement(name = "credit")
    public List<Credit> getListCredits() {
        return listCredits;
    }

    public void setListCredits(List<Credit> listCredits) {
        this.listCredits = listCredits;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (this.inn != null ? this.inn.hashCode() : 0);
        hash = 29 * hash + (this.serialPassport != null ? this.serialPassport.hashCode() : 0);
        hash = 29 * hash + (this.numberPassport != null ? this.numberPassport.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Client other = (Client) obj;
        if ((this.inn == null) ? (other.inn != null) : !this.inn.equals(other.inn)) {
            return false;
        }
        if ((this.serialPassport == null) ? (other.serialPassport != null) : !this.serialPassport.equals(other.serialPassport)) {
            return false;
        }
        if ((this.numberPassport == null) ? (other.numberPassport != null) : !this.numberPassport.equals(other.numberPassport)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Client{"
                + "inn=" + inn
                + ", firstName=" + firstName
                + ", secondName=" + secondName
                + ", lastName=" + lastName
                + ", dateBorn=" + dateBorn
                + ", serialPassport=" + serialPassport
                + ", numberPassport=" + numberPassport + '}';
    }
}
