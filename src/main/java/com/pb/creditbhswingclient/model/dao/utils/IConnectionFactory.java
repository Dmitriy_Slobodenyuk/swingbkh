package com.pb.creditbhswingclient.model.dao.utils;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IConnectionFactory {

    public Connection getConnection() throws SQLException;
}
