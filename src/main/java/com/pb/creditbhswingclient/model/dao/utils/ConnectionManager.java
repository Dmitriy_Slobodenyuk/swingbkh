package com.pb.creditbhswingclient.model.dao.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionManager {

    private Connection connection;
    private String host;
    private int port;
    private String userName;
    private String password;
    private String dbName;

    public ConnectionManager(String host,
            int port,
            String userName,
            String password,
            String dbName) {
        this.host = host;
        this.port = port;
        this.userName = userName;
        this.password = password;
        this.dbName = dbName;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public Connection getConnection() {
        if (connection == null) {
            connection = openConnection();
        }

        return connection;
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private Connection openConnection() {
        StringBuilder url = new StringBuilder("jdbc:sybase:Tds:")
                .append(host)
                .append(":")
                .append(port)
                .append("/")
                .append(dbName);

        try {
            return DriverManager.getConnection(url.toString(), userName, password);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }
}
