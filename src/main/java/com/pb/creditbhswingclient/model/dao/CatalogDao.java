package com.pb.creditbhswingclient.model.dao;

import com.pb.creditbhswingclient.model.dao.utils.ConnectionFactory;
import com.pb.creditbhswingclient.model.dao.utils.DaoConfig;
import com.pb.creditbhswingclient.model.dao.utils.IConnectionFactory;
import com.pb.creditbhswingclient.model.entity.Bank;
import com.pb.creditbhswingclient.model.entity.Currency;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CatalogDao {

    private BankCreator bankCreator;
    private CurrencyCreator currencyCreator;

    public CatalogDao() {
        currencyCreator = new CurrencyCreator();
        bankCreator = new BankCreator();
    }

    public Bank getBank(String bankCode) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Bank bank = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Bank_Dict WHERE BNK_CODE=?");
            statement.setString(1, bankCode);
            ResultSet set = statement.executeQuery();
            bank = bankCreator.formBank(set);
        } catch (SQLException e) {
            System.out.println(e);
            bank = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return bank;
    }

    public Bank getBankByName(String bankName) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Bank bank = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Bank_Dict WHERE BNK_NAME=?");
            statement.setString(1, bankName);
            ResultSet set = statement.executeQuery();
            bank = bankCreator.formBank(set);
        } catch (SQLException e) {
            System.out.println(e);
            bank = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return bank;
    }

    public Currency getCurrency(String currencyCode) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        IConnectionFactory connectionFactory1 = new ConnectionFactory();
        Currency currency = null;
        Connection connection = null;
        try {
            connection = connectionFactory1.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Cash_Dict WHERE CSH_ID=?");
            statement.setString(1, currencyCode);
            ResultSet set = statement.executeQuery();
            currency = currencyCreator.formCurrency(set);
        } catch (SQLException e) {
            System.out.println(e);
            currency = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return currency;
    }

    public Currency getCurrencyByName(String currencyName) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Currency currency = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM Cash_Dict WHERE CSH_NAM=?");
            statement.setString(1, currencyName);
            ResultSet set = statement.executeQuery();
            currency = currencyCreator.formCurrency(set);
        } catch (SQLException e) {
            currency = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return currency;
    }

}

class BankCreator {

    public BankCreator() {

    }

    public Bank formBank(ResultSet rs) throws SQLException {
        Bank bank = null;
        while (rs.next()) {
            bank = getBank(rs);
        }
        return bank;
    }

    private Bank getBank(ResultSet rs) throws SQLException {
        Bank bank = new Bank();
        bank.setBankCode(rs.getString("BNK_CODE"));
        bank.setBankName(rs.getString("BNK_NAME"));
        return bank;
    }
}

class CurrencyCreator {

    public CurrencyCreator() {

    }

    public Currency formCurrency(ResultSet rs) throws SQLException {
        Currency currency = null;
        while (rs.next()) {
            currency = getCurrency(rs);
        }
        return currency;
    }

    private Currency getCurrency(ResultSet rs) throws SQLException {
        Currency currency = new Currency();
        currency.setCurrCode(rs.getString("CSH_ID"));
        currency.setCurrName(rs.getString("CSH_NAM"));
        return currency;
    }
}
