package com.pb.creditbhswingclient.model.dao;


import com.pb.creditbhswingclient.model.dao.interfaces.IClientDao;
import com.pb.creditbhswingclient.model.dao.utils.DaoConfig;
import com.pb.creditbhswingclient.model.dao.utils.IConnectionFactory;
import com.pb.creditbhswingclient.model.entity.Client;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ClientDaoJdbc implements IClientDao {

    
    private ClientCreator clientCreator;
    

    public ClientDaoJdbc() {

        clientCreator = new ClientCreator();
    }

    @Override
    public Client getByINN(String inn) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Client client = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Clients WHERE CLN_INN=?");
            statement.setString(1, inn);
            ResultSet set = statement.executeQuery();
            client = clientCreator.formClient(set);

        } catch (SQLException e) {
            System.out.println(e);
            client = null;
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        return client;
    }

    @Override
    public Client getByFIOandDateBorn(String firstName, String secondName, String lastName, String dateBorn) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Client client = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Clients "
                    + "WHERE CLN_NAM1=? AND CLN_NAM2=? AND CLN_NAM3=? AND CLN_BD=?");
            statement.setString(1, firstName);
            statement.setString(2, secondName);
            statement.setString(3, lastName);
            //java.sql.Date sqlDate = new java.sql.Date(dateBorn.getTime());
            statement.setString(4, dateBorn);
            ResultSet set = statement.executeQuery();
            client = clientCreator.formClient(set);
        } catch (SQLException e) {
            System.out.println(e);
            client = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return client;
    }

    @Override
    public Client getByDataPassport(String sPass, String nPass) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Client client = null;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM Clients WHERE CLN_PS=? AND CLN_PN=?");
            statement.setString(1, sPass);
            statement.setString(2, nPass);
            ResultSet set = statement.executeQuery();
            client = clientCreator.formClient(set);
        } catch (SQLException e) {
            System.out.println(e);
            client = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return client;
    }

    @Override
    public int save(Client client) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        int result = 0;
        Connection connection = null;
        try {
            client.setIdClient(getIdentityKey());
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Clients VALUES (?,?,?,?,?,?,?,?,?)");
            statement.setLong(1, client.getIdClient());
            statement.setString(2, client.getInn());
            statement.setString(3, client.getFirstName());
            statement.setString(4, client.getSecondName());
            statement.setString(5, client.getLastName());
            //java.sql.Date sqlDate = new java.sql.Date(client.getDateBorn().getTime());
            //statement.setDate(6, sqlDate);
            statement.setString(7, client.getSerialPassport());
            statement.setString(8, client.getNumberPassport());
            statement.setString(9, client.getImage());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            result = 0;
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return result;
    }

    /**
     * вызов процедуры в БД
     *
     * @return newId - id для вставки
     */
    private long getIdentityKey() {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        long newId = 0;
        final String call = "{call addID ?,?}";
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            CallableStatement cs = connection.prepareCall(call);
            //передача значения входного параметра
            cs.setString(1, "Clients");
            //регистрация возвращаемого значения
            cs.registerOutParameter(2, java.sql.Types.BIGINT);
            cs.execute();
            newId = cs.getLong(2);
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return newId;
    }
}

class ClientCreator {

    public Client formClient(ResultSet rs) throws SQLException {
        Client client = null;
        if (rs.next()) {
            client = getClient(rs);
        }
        return client;
    }

    public List<Client> formListClients(ResultSet rs) throws SQLException {
        List<Client> clients = new ArrayList<Client>();
        if (rs.next()) {
            clients.add(getClient(rs));
        }
        return clients;
    }

    private Client getClient(ResultSet rs) throws SQLException {
        Client client = new Client();
        client.setIdClient(rs.getLong("CLN_ID"));
        //client.setIdAdmin(rs.getInt("ADM_ID"));
        client.setInn(rs.getString("CLN_INN"));
        client.setFirstName(rs.getString("CLN_NAM1"));
        client.setSecondName(rs.getString("CLN_NAM2"));
        client.setLastName(rs.getString("CLN_NAM3"));
        //client.setDateBorn(rs.getDate("CLN_BD"));
        client.setSerialPassport(rs.getString("CLN_PS"));
        client.setNumberPassport(rs.getString("CLN_PN"));
        client.setImage(rs.getString("CLN_IMG"));
        return client;
    }
}
