package com.pb.creditbhswingclient.model.dao.interfaces;

import com.pb.creditbhswingclient.model.entity.Credit;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface ICreditDao {

    public List<Credit> getCredits(long idClient);

    public int save(Credit credit);

    public int update(Credit credit);
}
