package com.pb.creditbhswingclient.model.dao;

import com.pb.creditbhswingclient.model.dao.interfaces.ICreditDao;
import com.pb.creditbhswingclient.model.dao.utils.DaoConfig;
import com.pb.creditbhswingclient.model.dao.utils.IConnectionFactory;
import com.pb.creditbhswingclient.model.entity.Bank;
import com.pb.creditbhswingclient.model.entity.Client;
import com.pb.creditbhswingclient.model.entity.Credit;
import com.pb.creditbhswingclient.model.entity.Currency;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class CreditDaoJdbc implements ICreditDao {

    private CreditCreator creditCreator;
    

    public CreditDaoJdbc() {
        creditCreator = new CreditCreator();
    }

    @Override
    public List<Credit> getCredits(long idClient) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        Connection connection = null;
        List<Credit> list = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT c1.CRD_ID, c1.CLN_ID, "
                    + "c1.BNK_CODE, c1.CRD_BSUM,c1.CSH_ID,c1.CRD_BDATE,"
                    + "c1.CRD_CBODY,c1.CRD_ENDDATE,c1.CRD_OUT "
                    + "FROM Credits as c1, Clients as c2 WHERE c1.CLN_ID=? AND c1.CLN_ID=c2.CLN_ID");
            statement.setLong(1, idClient);
            ResultSet set = statement.executeQuery();
            list = creditCreator.formCredits(set);
        } catch (SQLException e) {
            System.out.println(e.fillInStackTrace());
            list = null;
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.fillInStackTrace());
            }
        }
        return list;
    }

    @Override
    public int update(Credit credit) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        int result = 0;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("UPDATE Credits SET BNK_CODE=?, "
                    + "CSH_ID=?, CRD_CBODY=?, CRD_ENDDATE=?, CRD_OUT=? WHERE CRD_ID=?");
            statement.setString(1, credit.getBank().getBankCode());
            statement.setString(2, credit.getCurrency().getCurrCode());
            statement.setBigDecimal(3, credit.getCreditCurrBody());
            //java.sql.Date sqlDate = new java.sql.Date(credit.getCreditEnd().getTime());
            //statement.setDate(4, sqlDate);
            statement.setString(5, credit.getOutDelay());
            statement.setLong(6, credit.getIdCredit());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            result = 0;
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return result;
    }

    @Override
    public int save(Credit credit) {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        credit.setIdCredit(getIdentityKey());
        int result = 0;
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement("INSERT INTO Credits VALUES (?,?,?,?,?,?,?,?,?)");
            statement.setLong(1, credit.getIdCredit());
            //statement.setLong(2, credit.getClient().getIdClient());
            statement.setString(3, credit.getBank().getBankCode());
            statement.setBigDecimal(4, credit.getBaseAmount());
            statement.setString(5, credit.getCurrency().getCurrCode());
            //java.sql.Date sqlDate = new java.sql.Date(credit.getCreditDateIssue().getTime());
            //statement.setDate(6, sqlDate);
            statement.setBigDecimal(7, credit.getCreditCurrBody());
            //java.sql.Date sqlDateEnd = new java.sql.Date(credit.getCreditEnd().getTime());
            //statement.setDate(8, sqlDateEnd);
            statement.setString(9, credit.getOutDelay());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            result = 0;
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return result;
    }

    /**
     * вызов процедуры в БД
     *
     * @return newId - id для сохранения нового кредита
     */
    private long getIdentityKey() {
        IConnectionFactory connectionFactory = DaoConfig.getConnectionFactory();
        long newId = 0;
        final String call = "{call addID ?,?}";
        Connection connection = null;
        try {
            connection = connectionFactory.getConnection();
            CallableStatement cs = connection.prepareCall(call);
            //передача значения входного параметра
            cs.setString(1, "Credits");
            //регистрация возвращаемого значения
            cs.registerOutParameter(2, java.sql.Types.BIGINT);
            cs.execute();
            newId = cs.getLong(2);
        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                if (connection != null) {
                    connection.close();
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
        return newId;
    }
}

class CreditCreator {

    private CatalogDao catalogDao;

    public CreditCreator() {
        catalogDao = new CatalogDao();
    }

    public List<Credit> formCredits(ResultSet rs) throws SQLException {
        List<Credit> credit = new ArrayList<>();
        while (rs.next()) {
            credit.add(getCredit(rs));
        }
        return credit;
    }

    private Credit getCredit(ResultSet rs) throws SQLException {
        Credit credit = new Credit();
        credit.setIdCredit(rs.getInt("CRD_ID"));
        Client client = new Client();
        client.setIdClient(rs.getLong("CLN_ID"));
        //credit.setClient(client);
        Bank bank = catalogDao.getBank(rs.getString("BNK_CODE"));
        credit.setBank(bank);
        credit.setBaseAmount(rs.getBigDecimal("CRD_BSUM"));
        Currency currency = catalogDao.getCurrency(rs.getString("CSH_ID"));
        credit.setCurrency(currency);
        //credit.setCreditDateIssue(rs.getDate("CRD_BDATE"));
        credit.setCreditCurrBody(rs.getBigDecimal("CRD_CBODY"));
        //credit.setCreditEnd(rs.getDate("CRD_ENDDATE"));
        credit.setOutDelay(rs.getString("CRD_OUT"));

        return credit;
    }
}
