package com.pb.creditbhswingclient.model.dao.utils;


import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Dmitriy Slobodenyuk
 */
public class ConnectionFactory implements IConnectionFactory {

    private ConnectionManager connect;
   

    public ConnectionFactory() {
        getDbConfig();
    }

    @Override
    public Connection getConnection() throws SQLException {
        return connect.getConnection();
    }

    private void getDbConfig() {
        Properties prop = new Properties();
        InputStream output = null;

        try {

            output = new FileInputStream("src/main/resources/dbconfig.properties");
            prop.load(output);

            String host = prop.getProperty("host");
            int port = Integer.parseInt(prop.getProperty("port"));
            String dbuser = prop.getProperty("dbuser");
            String password = prop.getProperty("password");
            String dbname = prop.getProperty("dbname");

            connect = new ConnectionManager(host, port, dbuser, password, dbname);

        } catch (IOException io) {
            System.out.println(io);
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    System.out.println(e);
                }
            }

        }

    }
}
