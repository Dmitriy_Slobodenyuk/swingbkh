package com.pb.creditbhswingclient.model.dao.interfaces;

import com.pb.creditbhswingclient.model.entity.Client;


/**
 *
 * @author Dmitriy Slobodenyuk
 */
public interface IClientDao {

    public Client getByINN(String inn);

    public Client getByFIOandDateBorn(String firstName, String secondName,
            String lastName, String dateBorn);

    public Client getByDataPassport(String sPass, String nPass);

    public int save(Client client);
}
