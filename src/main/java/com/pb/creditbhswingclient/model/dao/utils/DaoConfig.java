package com.pb.creditbhswingclient.model.dao.utils;

public class DaoConfig {

    private static IConnectionFactory connectionFactory;

    public static void init(IConnectionFactory connectionFactoryParam) {
        if (connectionFactory == null) {
            connectionFactory = connectionFactoryParam;
        }
    }

    public static IConnectionFactory getConnectionFactory() {
        connectionFactory = new ConnectionFactory();
        return connectionFactory;
    }
}
